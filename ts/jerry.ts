///<reference path="gameItem.ts" />
//jerry the main clickable character
class Jerry extends Charachters {

    constructor() {
        super('jerry', 1, 1);
    }

    public draw(): void {
        var container: HTMLElement = document.getElementById('middle');
        var img: HTMLImageElement = document.createElement('img');
        img.id = 'jerry';
        img.src = '../assets/jerry.png';
        container.appendChild(img);
    }
}
// little tommy the first unlock
class Tommy extends Charachters {

    constructor() {
        super('tom', 2, 10);
    }
    public draw(): void {
        var container: HTMLElement = document.getElementById('container');
        var div: HTMLElement = document.createElement('div');
        div.id = 'side';
        var img: HTMLImageElement = document.createElement('img');
        img.className = 'enemies';
        img.id = 'tommy';
        img.src = '../assets/babyTom.png';
        var p:HTMLParagraphElement = document.createElement('p');
        p.innerHTML = "Little tommy would like to help you slap jerry, click on jerry to slap him for 10x per click!";
        container.appendChild(div);
        div.appendChild(img);
        div.appendChild(p);
    }
}
//Tom the classic antagonist of jerry
class Tom extends Charachters{

    constructor() {
        super('tom', 3 , 50);

    }
    public draw(): void{
        var container: HTMLElement = document.getElementById('container');
        var div: HTMLElement = document.getElementById('side');
        var img: HTMLImageElement = document.createElement('img');
        img.className = 'enemies';
        img.id = 'tom';
        img.src = '../assets/tom.png';
        var p:HTMLParagraphElement = document.createElement('p');
        p.innerHTML = "Tom sees how much fun little tommy is having and decides to get a hammer and join in, click on jerry to slap him for 50x per click!";
        div.appendChild(img);
        div.appendChild(p);
    }

}
// Wile E. Coyote who normally hunts roadrunner, now slaps jerry for a living
class Willy extends Charachters{

    constructor(){
        super('willy',4, 1000);
    }

    public draw():void{
        var container: HTMLElement = document.getElementById('container');
        var div: HTMLElement = document.getElementById('side');
        var img: HTMLImageElement = document.createElement('img');
        img.className = 'enemies';
        img.id = 'willy';
        img.src = '../assets/willy.jpg';
        var p:HTMLParagraphElement = document.createElement('p');
        p.innerHTML = "Out of nowhere comes Wile E. coyote, and het brought the big guns! Clicking jerry now gives 1000 slaps.";
        div.appendChild(img);
        div.appendChild(p);
    }
}
