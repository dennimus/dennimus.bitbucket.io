/** Class representing a character */
class Charachters {

    private name: string;
    private id: number;
    private slaps: number;

    /**
    * Create a Character.
    * @param {string} name - The name value.
    * @param {number} id - The id value.
    * @param {number} slaps - The number of slaps the character gives you
    */
    constructor(name: string, id: number, slaps: number) {
        this.name = name;
        this.id = id;
        this.slaps = slaps;
    }
    
    public numberOfSlaps(): number{
        return this.slaps;
    }
}