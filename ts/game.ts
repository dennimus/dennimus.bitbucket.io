/** Class representing a game */
class Game {
    private gameItems: Charachters[];
    private jerry: Jerry;
    private tommy: Tommy;
    private tom: Tom;
    private willy: Willy;
    private img: HTMLElement;
    private p: HTMLParagraphElement;
    private score: number;
    private tommyTime: boolean;
    private hammerTime: boolean;
    private willyTime: boolean;
    
    /**
     * Create a Game.
     * @param {array} _holes - The score value.
    */
    constructor() {
        this.jerry = new Jerry();
        this.gameItems = [];
        this.gameItems.push(this.jerry);
        this.score = 0;
        this.tommyTime = false;
        this.hammerTime = false;
        this.willyTime = false;
        this.p = document.createElement('p')
        this.render();
        this.img = document.getElementById('jerry')
        this.img.addEventListener('click', this.clickHandler);
    }

    /**
     * Renders the game representation in the DOM
     */
    private render() {
        this.jerry.draw();
        var middle: HTMLElement = document.getElementById('middle');
        var scoreString = this.score.toString();
        this.p.innerHTML = scoreString;
        middle.appendChild(this.p);
    }
    /**
     * Handles the mouseclick
     */
    private clickHandler = (e: MouseEvent) => {
        var slaps:number = this.gameItems[0].numberOfSlaps();
        if (this.tommyTime) {
            slaps = this.gameItems[1].numberOfSlaps();
        }
        else if(this.hammerTime){
            slaps = this.gameItems[2].numberOfSlaps();
        }
        else if(this.willyTime){
            slaps = this.gameItems[3].numberOfSlaps();
        }
        this.score += slaps;
        // console.log(this.score);
        if (this.score >= 50 && !this.tommyTime && !this.hammerTime && !this.willyTime) {
            this.addTommy();
            this.score = 0;
        }
        if(this.score >= 500 && !this.hammerTime && !this.willyTime){
            this.addTom();
            this.score = 0;
        }
        if(this.score >= 5000 && !this.willyTime){
            this.addWilly();
            this.score = 0;
        }
        if(this.score > 1000000000000000 ){
            alert("Now you killed him! great job!, I bet it was Wile who gave him the last slap. Let me revive him for you, so you can continue slapping him!")
            this.score = 0;
        }
        this.p.innerHTML = this.score.toString();
    }
    /**
     * Adds little tommy to the game
     */
    private addTommy():void {
        this.tommy = new Tommy();
        this.gameItems.push(this.tommy);
        this.tommy.draw();
        this.tommyTime = true;
    }
    /**
     * Adds Tom to the game
     */
    private addTom():void{
        this.tom = new Tom();
        this.gameItems.push(this.tom);
        this.tom.draw();
        this.hammerTime = true;
        this.tommyTime = false;
    }
    /**
     * Adds Wile E. Coyote to the game
     */
    private addWilly():void{
        this.willy = new Willy();
        this.gameItems.push(this.willy);
        this.willy.draw();
        this.willyTime = true;
        this.hammerTime = false;
    }

}