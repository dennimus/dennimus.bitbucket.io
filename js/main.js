var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var app;
(function () {
    var init = function () {
        app = new Game();
    };
    window.addEventListener('load', init);
})();
var Game = (function () {
    function Game() {
        var _this = this;
        this.clickHandler = function (e) {
            var slaps = _this.gameItems[0].numberOfSlaps();
            if (_this.tommyTime) {
                slaps = _this.gameItems[1].numberOfSlaps();
            }
            else if (_this.hammerTime) {
                slaps = _this.gameItems[2].numberOfSlaps();
            }
            else if (_this.willyTime) {
                slaps = _this.gameItems[3].numberOfSlaps();
            }
            _this.score += slaps;
            if (_this.score >= 50 && !_this.tommyTime && !_this.hammerTime && !_this.willyTime) {
                _this.addTommy();
                _this.score = 0;
            }
            if (_this.score >= 500 && !_this.hammerTime && !_this.willyTime) {
                _this.addTom();
                _this.score = 0;
            }
            if (_this.score >= 5000 && !_this.willyTime) {
                _this.addWilly();
                _this.score = 0;
            }
            if (_this.score > 1000000000000000) {
                alert("Now you killed him! great job!, I bet it was Wile who gave him the last slap. Let me revive him for you, so you can continue slapping him!");
                _this.score = 0;
            }
            _this.p.innerHTML = _this.score.toString();
        };
        this.jerry = new Jerry();
        this.gameItems = [];
        this.gameItems.push(this.jerry);
        this.score = 0;
        this.tommyTime = false;
        this.hammerTime = false;
        this.willyTime = false;
        this.p = document.createElement('p');
        this.render();
        this.img = document.getElementById('jerry');
        this.img.addEventListener('click', this.clickHandler);
    }
    Game.prototype.render = function () {
        this.jerry.draw();
        var middle = document.getElementById('middle');
        var scoreString = this.score.toString();
        this.p.innerHTML = scoreString;
        middle.appendChild(this.p);
    };
    Game.prototype.addTommy = function () {
        this.tommy = new Tommy();
        this.gameItems.push(this.tommy);
        this.tommy.draw();
        this.tommyTime = true;
    };
    Game.prototype.addTom = function () {
        this.tom = new Tom();
        this.gameItems.push(this.tom);
        this.tom.draw();
        this.hammerTime = true;
        this.tommyTime = false;
    };
    Game.prototype.addWilly = function () {
        this.willy = new Willy();
        this.gameItems.push(this.willy);
        this.willy.draw();
        this.willyTime = true;
        this.hammerTime = false;
    };
    return Game;
}());
var Charachters = (function () {
    function Charachters(name, id, slaps) {
        this.name = name;
        this.id = id;
        this.slaps = slaps;
    }
    Charachters.prototype.numberOfSlaps = function () {
        return this.slaps;
    };
    return Charachters;
}());
var Jerry = (function (_super) {
    __extends(Jerry, _super);
    function Jerry() {
        return _super.call(this, 'jerry', 1, 1) || this;
    }
    Jerry.prototype.draw = function () {
        var container = document.getElementById('middle');
        var img = document.createElement('img');
        img.id = 'jerry';
        img.src = '../assets/jerry.png';
        container.appendChild(img);
    };
    return Jerry;
}(Charachters));
var Tommy = (function (_super) {
    __extends(Tommy, _super);
    function Tommy() {
        return _super.call(this, 'tom', 2, 10) || this;
    }
    Tommy.prototype.draw = function () {
        var container = document.getElementById('container');
        var div = document.createElement('div');
        div.id = 'side';
        var img = document.createElement('img');
        img.className = 'enemies';
        img.id = 'tommy';
        img.src = '../assets/babyTom.png';
        var p = document.createElement('p');
        p.innerHTML = "Little tommy would like to help you slap jerry, click on jerry to slap him for 10x per click!";
        container.appendChild(div);
        div.appendChild(img);
        div.appendChild(p);
    };
    return Tommy;
}(Charachters));
var Tom = (function (_super) {
    __extends(Tom, _super);
    function Tom() {
        return _super.call(this, 'tom', 3, 50) || this;
    }
    Tom.prototype.draw = function () {
        var container = document.getElementById('container');
        var div = document.getElementById('side');
        var img = document.createElement('img');
        img.className = 'enemies';
        img.id = 'tom';
        img.src = '../assets/tom.png';
        var p = document.createElement('p');
        p.innerHTML = "Tom sees how much fun little tommy is having and decides to get a hammer and join in, click on jerry to slap him for 50x per click!";
        div.appendChild(img);
        div.appendChild(p);
    };
    return Tom;
}(Charachters));
var Willy = (function (_super) {
    __extends(Willy, _super);
    function Willy() {
        return _super.call(this, 'willy', 4, 1000) || this;
    }
    Willy.prototype.draw = function () {
        var container = document.getElementById('container');
        var div = document.getElementById('side');
        var img = document.createElement('img');
        img.className = 'enemies';
        img.id = 'willy';
        img.src = '../assets/willy.jpg';
        var p = document.createElement('p');
        p.innerHTML = "Out of nowhere comes Wile E. coyote, and het brought the big guns! Clicking jerry now gives 1000 slaps.";
        div.appendChild(img);
        div.appendChild(p);
    };
    return Willy;
}(Charachters));
//# sourceMappingURL=main.js.map